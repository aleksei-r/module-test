package module_test

import (
	"fmt"
	_ "github.com/google/uuid"
	_ "github.com/labstack/echo/v4"
)

func ModuleTest() {
	fmt.Println("Module-Test enabled!")
}
