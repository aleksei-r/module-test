module bitbucket.org/aleksei-r/module-test

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/labstack/echo/v4 v4.1.14
)
